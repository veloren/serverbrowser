#![feature(let_chains)]

use std::{
    fs::read_to_string,
    net::{IpAddr, SocketAddr},
    str::FromStr,
};

use veloren_query_server::client::QueryClient;
use veloren_serverbrowser_api::GameServerList;

#[tokio::main]
async fn main() {
    let file = std::env::args()
        .nth(1)
        .expect("Expected server list file as argument");

    let servers: GameServerList = ron::from_str(&read_to_string(&file).unwrap()).unwrap();

    for server in servers.servers {
        let (port, had_port) = server
            .query_port
            .map_or_else(|| (14006, false), |p| (p, true));

        let addr = if let Ok(ip) = IpAddr::from_str(&server.address) {
            SocketAddr::new(ip, port)
        } else {
            let ips = tokio::net::lookup_host((server.address.as_str(), port)).await;

            if let Ok(mut ips) = ips
                && let Some(ip) = ips.next()
            {
                ip
            } else {
                eprintln!(
                    "Could not get socket addresss for server: {}",
                    server.address
                );
                continue;
            }
        };

        let mut client = QueryClient::new(addr);
        let info = client.server_info().await;

        if info.is_err() && had_port {
            eprintln!("Query server no longer reachable: {}", server.address);
        } else if info.is_ok() && !had_port {
            eprintln!(
                "Query server reachable via default port: {}",
                server.address
            );
        }
    }
}
