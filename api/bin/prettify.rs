//! Reads the provided servers.ron file and updates it with a prettified
//! version.

use veloren_serverbrowser_api::GameServerList;

fn main() {
    let path = std::env::args()
        .nth(1)
        .unwrap_or_else(|| "server/servers.ron".to_string());

    let servers: GameServerList = ron::from_str(
        &std::fs::read_to_string(&path).expect("Could not read the servers.ron file"),
    )
    .unwrap();
    let formatted = ron::ser::to_string_pretty(&servers, Default::default())
        .expect("Serializing prettified server list failed");
    std::fs::write(&path, formatted).expect("Writing formatted server list failed");
}
