# Add a server to the server browser

<!--
  Fields preceded by an asterisk in parentheses are mandatory.
  It is not necessary to fill all or any of the optional fields.
-->

## Server information

- (*) Name:
- Description:
  <!-- An example of a server hostname: veloren.example.com -->
- (*) Hostname:
  <!--
    The default game server port is '14004'.
    The default game server protocol is 'TCP', alternatives are 'UDP' and 'QUIC'.
    The default web port is '14005'.
    The web protocol is always 'TCP'.
    The default info query port is '14006'.
    The info query protocol is 'UDP'.
    The ports are those exposed to the Internet.
  -->
- (*) Network ports:
  - Game server:
    - Port:
    - Info query port:
    - Protocol:
  - Web:
    - Port:
  <!--
    The location must be an ISO-3166-1 Alpha-2 code.
    See https://en.wikipedia.org/wiki/List_of_ISO_3166_country_codes
  -->
- (*) Location:

## Contact information

<!--
  This information is not published on Airshipper.
  We use it to contact the server owner when an incident arises.

  Matrix usernames must be given in complete form, including the server part.
    For example: @johndoe:matrix.org
  If you specify a simpler username, it will be considered a Discord username.
    For example: johndoe
-->
- (*) Contact email:
- (*) Your Discord or Matrix username:

## Social media and websites

- Website:
- Discord:
- Matrix:
- YouTube:
- Twitch:
- Mastodon:
- Other:

## Acknowledgement

<!-- Your explicit acknowledgement is needed. -->

- [ ] I hereby confirm that the server complies with all the requirements set
in the [README](/README.md#requirements).

<!-- Ignore the lines below. -->
/label ~server_request
